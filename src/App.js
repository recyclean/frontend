
import React from "react"

//import logo from './logo.svg';
import './App.css';
import Header from './component/Header';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './component/pages/Home';

import ManageUsers from './component/pages/ManageUsers';
import admin from './component/pages/admin';
import Registration from './component/auth/Registration';
import Login from './component/auth/Login';
import Product from "./component/pages/Product";
import ScanQR from "./component/scan/ScanQR";
import offer from "./component/pages/offer";


function App(props) {
    return (
        <BrowserRouter>
          <div>
            <Header />
            <Switch>
              <Route exact path="/" component={Home}  />
              <Route path="/registration" component={Registration} />
              <Route path="/login" component={Login} />
              <Route path="/manageUsers" component={ManageUsers} />
              <Route path="/admin" component={admin} />
              <Route path="/products" component={Product} />
              <Route path="/ScanQR" component={ScanQR} />
              <Route path="/offer" component={offer} />

            </Switch>
          </div>
        </BrowserRouter>

    );
  }


export default App;
