import React, { useState } from 'react';
import { useAuth } from "./context/auth";
import { Redirect } from "react-router-dom";
import axios from 'axios';

function Login() {
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [isError, setIsError] = useState(false);
    const [userEmail, setUserEmail] = useState("");
    const [userPass, setUserPass] = useState("");
    const { setAuthTokens } = useAuth();

    const LOGIN_API = 'http://localhost:8080/recyclean/user/login';
    var user = {
        userEmail: userEmail,
        userPass: userPass
    }

    function postLogin() {
        axios.post(LOGIN_API, user).then(
            result => {
                if (result.status === 200) {
                    setAuthTokens(result.data);
                    setLoggedIn(true);
                } else {
                    setIsError(true);
                }
            }
        ).catch(
            e => {
                setIsError(true);
            }
        )
    }

    if (isLoggedIn) {
        return <Redirect to="/" />
    }

    return (
        <div>
            <h1>Login 2</h1>
            <div className="container">

                {isError && <div class="alert alert-danger">The username or password provided were incorrect!</div>}


                <div id="formContent">
                    <form onSubmit={postLogin()}>
                        <input
                            type="text"
                            id="email"
                            className="input"
                            name="userEmail"
                            placeholder="Email"
                            value={userEmail}
                            onChange={e => {
                                setUserEmail(e.target.value)
                            }}
                            required
                        />


                        <input
                            type="password"
                            id="password"
                            className="input"
                            name="userPass"
                            placeholder="Password"
                            value={userPass}
                            onChange={e => {
                                setUserPass(e.target.value)
                            }}
                            required
                        />
                        <input type="submit" value="Login" />
                    </form>
                </div>
            </div>
        </div>
    )
}


export default Login;