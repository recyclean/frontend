import React, { Component } from "react";
import axios from 'axios'


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_Id: 777,
      waste_history: []
    }
  }

  getHistoryWaste() {
    const URL_WASTE_HIST = `http://localhost:8080/recyclean/recycle/history/${this.state.user_Id}`;
    axios.get(URL_WASTE_HIST).then(
      this.setState({
        
      }
      )
    );
  }


  render() {
    return (
      <div>
        <div>
        {/*==================================================================
			Preloader
		==================================================================*/}
        <div id="preloader">
          <div className="loader">
            <img src="img/loader.gif" alt="" />
          </div>
        </div>
        {/*==================================================================
			Main Wrapper
		==================================================================*/}
        <div id="main-wrapper">
          {/*==================================================================
				Intro Section
			==================================================================*/}
          <section id="intro" className="section intro-section">
            <div className="container">
              <div className="row intro-cols">
                <div className="col-md-6 col-md-push-6">
                  <div className="phone">
                    <div className="phone-img">
                      <img src="img/phone1.png" alt="" />
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-md-pull-6">
                  <div className="v-align">
                    <div className="inner">
                      <div className="intro-text">
                        <h1>
                          Mi App
                        </h1>
                        <p>
                          This is the coolest app you've ever seen, the amazing and great app made with love just for you
                        </p>
                        <div className="intro-download-btns">
                          <a href="#" className="app-btn">
                            <img src="img/appstore.png" alt="" />
                          </a>
                          <a href="#" className="app-btn">
                            <img src="img/googleplay.png" alt="" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div></div>














        <table>
        <ul>
                    {this.state.waste_history.map(item => (
                        <li key={item}>{item}</li>
                    ))}
        </ul>
        </table>
      </div>



    );
  }
}
export default Home;