import React, { Component } from 'react'
import QrReader from 'react-qr-reader'
import axios from 'axios'




class ScanQR extends Component {
    constructor(props) {
        super(props);
        this.state = {
            last_scanned: 'No Result',
            products_scanned: []
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleScan = data => {
        if (data !== null) {
            console.log('code scanned = ' + data);
            if (Number.isInteger(parseInt(data, 10))) {
                if (this.state.products_scanned.includes(data)) {
                    alert("This code has been already scanned, please try another code");

                }
                else {
                    this.setState(state => {
                        const products_scanned = state.products_scanned.concat(data);
                        return {
                            products_scanned,
                            result: 'No Result',
                            message: 'No Result',
                        };
                    });
                    //alert("Yay 400 Credits")
                    console.log('code scanned = ' + data);
                }
            }
        }
    }
    handleError = err => {
        console.error(err)
    }


    /*
        User click button [add to list]
            button[add To list]:
                add to list
            button[submit list]:
                *send Json
                    {
                        Prod_Id[List]
                        email
                    }   

                recyclean/credits/update
                    
                    getUserWasteHistory()
                    getuserCredits()
                    getUserLogin()


        */

    handleSubmit(event) {
        var API_URL = 'http://localhost:8080/recyclean/credits/update';
        axios.post(API_URL,
            {
                productIds: [21, 12],
                user_Id: 123,
                user_Email: "re@yahoo.com"
            }
        ).then(
            response => {
                if (response.data) {
                    this.setState(
                        {

                        }

                    )
                    alert("Response update credits succes");
                }
            }
        );

    }

    addToList = () => {
        this.setState(state => {
            const products_scanned = state.products_scanned.concat(state.last_scanned);
            return {
                products_scanned,
                result: 'No Result',
                message: 'No Result',
            };
        });
    }

    render() {
        return (
            <div>
                <div>
                    <div>
                        {/*==================================================================
			Preloader
		==================================================================*/}
                        <div id="preloader">
                            <div className="loader">
                                <img src="img/loader.gif" alt="" />
                            </div>
                        </div>
                        {/*==================================================================
			Main Wrapper
		==================================================================*/}
                        <div id="main-wrapper">
                            {/*==================================================================
				Intro Section
			==================================================================*/}
                            <section id="intro" className="section intro-section">
                                <div className="container">
                                    <div className="row intro-cols">
                                        <div className="col-md-6 col-md-pull-6">
                                            <div className="v-align">
                                                <div className="inner">
                                                    <div className="intro-text">

                                                        <h3>Scan your code now and earn Credits!</h3>
                                                        <p>
                                                            This is the coolest app you've ever seen, the amazing and great app made with love just for you
                        </p>

                                                        <ul>

                                                            {this.state.products_scanned.map(item => (
                                                                <div class="alert alert-success"><li key={item}>{item}</li></div>
                                                            ))}

                                                        </ul>
                                                        <br></br>
                                                        <br></br>
                                                        <br></br>
                                                        <br></br>
                                                    </div>









                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-md-push-6">
                                            <div className="">
                                                <div className="">

                                                    <div className="container ScanReader">
                                                        <QrReader
                                                            delay={50}
                                                            onError={this.handleError}
                                                            onScan={this.handleScan}
                                                            style={{ width: '78%' 
                                                            
                                                        }}
                                                        />
                                                    </div>


                                                </div>

                                            </div>
                                            <div className="intro-download-btns">
                                                <button type="submit" className="btn app-btn" onClick={this.addToList}>Scan This Code</button>
                                                <button type="submit" className="btn app-btn" onClick={this.handleSubmit}>Send all </button>
                                            </div>
                                            <br></br>
                                            <br></br>
                                            <br></br>
                                            <br></br>
                                        </div>



                                    </div>
                                </div>
                            </section>
                        </div></div></div>







            </div>
        )
    }
}
export default ScanQR