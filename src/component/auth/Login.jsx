import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userEmail: "",
            userPass: "",
            loginErrors: ""
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        console.log('handle change', event)
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleSubmit(event) {
        var API_URL = 'http://localhost:8080/recyclean/user/login';


        let user = this.state

        axios.post(API_URL, user)
            .then(
                
                response => {
                    if(response.data){
                    this.setState(
                        {
                            loginErrors:'',
                            message: `You are now connected as ${user.userEmail} `
                        }
                    )
                    this.props.history.push('/');
                }
                    else
                    this.setState(
                        {
                            message: '',
                            loginErrors: 'Wrong Email/Password ! Try again'
                        }
                    )
                }
            );

        // so that the form doesnt behave like an HTML form
        event.preventDefault();
    }



    render() {
        return (
            <div>
                <div className="container">
                    {this.state.message && <div class="alert alert-success">{this.state.message}</div>}
                    {this.state.loginErrors && <div class="alert alert-danger">{this.state.loginErrors}</div>}
                    <div id="formContent">
                        <form onSubmit={this.handleSubmit}>
                            <input
                                type="text"
                                id="email"
                                className="input"
                                name="userEmail"
                                placeholder="Email"
                                value={this.state.userEmail}
                                onChange={this.handleChange}
                                required
                            />


                            <input
                                type="password"
                                id="password"
                                className="input"
                                name="userPass"
                                placeholder="Password"
                                value={this.state.userPass}
                                onChange={this.handleChange}
                                required
                            />
                            <input type="submit" value="Login" />
                        </form>
                    </div>
                </div>
            </div>
        )
    }


}