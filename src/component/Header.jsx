import React, { Component } from 'react';
import Logo from '../img/Logo.png';

class Header extends Component {
    render() {
        return (
            <header className="header">
                <div className="logo">
                    <div className="logo"><img src={Logo} height="180" width="180" alt=""></img>
                    </div>
                </div>
                <div className="container">
                    <nav>
                        <ul className="navbar">
                            <li className="nav-item"><a href="/">Home</a></li>
                            <li className="nav-item"><a href="/offer">Offer</a></li>
                            <li className="nav-item"><a href="/about">About Us</a></li>
                            <li className="nav-item"><a href="/service">Services</a></li>
                            <li className="nav-item"><a href="/contact">Contact</a></li>
                            <li className="nav-item"><a href="/login">Login</a></li>
                            <li className="nav-item"><a href="/registration">Sign Up</a></li>
                            <li className="nav-item"><a href="/manageUsers">Manage Users</a></li>
                            <li className="nav-item"><a href="/products">My Products</a></li>
                        </ul>
                    </nav>
                </div>
            </header>
        )
    }
}

export default Header
